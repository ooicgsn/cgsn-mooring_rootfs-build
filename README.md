#CGSN Mooring Rootfs Build

This codebase `cgsn-mooring_rootfs-build` is for generating the root filesystem for the SD card on the CGSN Mooring computers.

The documentation about use of this codebase is maintained here: [https://bitbucket.org/ooicgsn/cgsn-mooring/src/master/src/doc/markdown/doc_02-embedded-image.md](https://bitbucket.org/ooicgsn/cgsn-mooring/src/master/src/doc/markdown/doc_02-embedded-image.md).

