#!/bin/bash -e
# Copyright (C) 2019 Woods Hole Oceanographic Institution
#
# This file is part of the CGSN Mooring Project ("cgsn-mooring").
#
# cgsn-mooring is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# cgsn-mooring is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cgsn-mooring in the COPYING.md file at the project root.
# If not, see <http://www.gnu.org/licenses/>.
################################################################################
# This tool installs Debian packages into the rootfs volume of an existing disk
# image. Create such a disk image with master_raspi_base_image.sh
################################################################################

shopt -s nullglob
. "$(cd "$(dirname "$0")"; pwd)"/includes/image_utils.sh


# Set up an exit handler to clean up after ourselves
function finish {
  ( # Run in a subshell to ignore errors
    set +e
    
    # Undo changes to the binfmt configuration
    reset_binfmt_rules
    
    # Remove the bind mounts
    if [ -n "$PKG_NAME" ]; then
      sudo umount "$ROOTFS_PARTITION"/"$PKG_NAME"
      rm -f "$ROOTFS_PARTITION"/"$PKG_NAME"
    fi
    if [ -n "$INTERPRETER" ]; then
      sudo umount "$ROOTFS_PARTITION"/"$INTERPRETER"
      rm -f "$ROOTFS_PARTITION"/"$INTERPRETER"
    fi
  
    # Unmount the rootfs partition
    sudo umount "$ROOTFS_PARTITION"
    rm -Rf "$ROOTFS_PARTITION"

    # Detach the loop devices
    detach_image "$SD_IMAGE_PATH"
  ) &>/dev/null || true 
}
trap finish EXIT


# Parse command-line options
if [[ $# -lt 2 ]]; then
  echo "Usage: $0 image.img package.deb [package.deb ...]" 2>&1
  exit 1
fi

SD_IMAGE_PATH="$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
shift
PACKAGES=("$@")


# Test that executing foreign binaries under QEMU will work
if ! enable_binfmt_rule qemu-aarch64; then
  echo "This system cannot execute ARM binaries under QEMU" >&2
  exit 1
fi


# Set up loop device for the partitions
attach_image "$SD_IMAGE_PATH" BOOT_DEV ROOTFS_DEV

# Mount the rootfs partition
ROOTFS_PARTITION="$(mktemp -d)"
sudo mount "$ROOTFS_DEV" "$ROOTFS_PARTITION"

# Bind the QEMU interpreter into the chroot
INTERPRETER=$(binfmt_interpreter qemu-aarch64)
touch "$ROOTFS_PARTITION"/"$INTERPRETER"
sudo mount -o bind "$INTERPRETER" "$ROOTFS_PARTITION"/"$INTERPRETER"

# Install the packages into the image
for PKG in "${PACKAGES[@]}"; do
  PKG_NAME="$(basename "$PKG")"
  echo "Installing $PKG_NAME..."
  touch "$ROOTFS_PARTITION"/"$PKG_NAME"
  sudo mount -o bind "$PKG" "$ROOTFS_PARTITION"/"$PKG_NAME"
  sudo chroot "$ROOTFS_PARTITION" dpkg -i /"$PKG_NAME"
  sudo umount "$ROOTFS_PARTITION"/"$PKG_NAME"
  rm -f "$ROOTFS_PARTITION"/"$PKG_NAME"
  unset PKG_NAME
done

# Fin.
echo "Completed."
